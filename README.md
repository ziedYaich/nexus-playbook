# Nexus Install Playbook   

This playbook will install the latest Nexus OSS repository (Version 3.x)
by using the following roles:

- https://gitlab.com/ziedYaich/oracle-java.git
- [geerlingguy.apache](https://galaxy.ansible.com/geerlingguy/apache/)
- https://gitlab.com/ziedYaich/nexus3-oss.git

## Steps to use

Install the required roles by executing the following command in the ansible control machine:

```yaml
    ansible-galaxy install -r requirements.yml
```
Execute the playbook against the host you intend to install nexus within

```yaml
    ansible-playbook -l [HOST] nexus-playbook.yml --extra-vars "ansible_become_pass=[PASSWORD]"
```
